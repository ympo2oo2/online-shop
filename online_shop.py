from app import app, db
from app.models import User, StockItem, BookedItem, Comment


@app.shell_context_processor
def make_shell_context():
    return {
        'db': db, 
        'User': User,
        'StockItem': StockItem,
        'BookedItem': BookedItem,
        'Comment': Comment,
    }
