from flask_wtf import FlaskForm
from flask_login import current_user
from wtforms import StringField, PasswordField, BooleanField, \
                    SubmitField, RadioField, SelectField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo
from app.models import User


class LoginForm(FlaskForm):
    login = StringField('Login', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


class RegistrationForm(FlaskForm):
    login = StringField('Login', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Repeat Password',
        validators=[DataRequired(), EqualTo('password')])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Register Account')

    # validate_<field_name> are addition validators for eponymous fields
    def validate_login(self, login):
        user = User.query.filter_by(login=login.data).first()
        if user is not None:
            raise ValidationError('Login is incorrect or already in use.')


class UserEditInfoForm(FlaskForm):
    login = StringField('Login', validators=[DataRequired()])
    first_name = StringField('First Name')
    second_name = StringField('Second Name')
    email = StringField('Email', validators=[DataRequired()])
    post_address = StringField('Post Address')
    post_address_index = StringField('Post Address Index')
    submit = SubmitField('Apply Changes')

    def validate_login(self, login):
        user = User.query.filter_by(login=login.data).first()
        if user is not None and user != current_user:
            raise ValidationError('Login is already in use!')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None and user != current_user:
            raise ValidationError('Email is already in use.')

    def validate_post_address_index(self, post_address_index):
        if len(str(post_address_index.data)) != 6:
            raise ValidationError('Post address index should be 6 digits length.')


class ResetUserPasswordForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired, Email()])
    submit = SubmitField('Reset Password')


class OrderItemForm(FlaskForm):
    color = RadioField('Color',
        choices=[('B','Black'), ('W','White'), ('G','Gold'), ('C','Cream'), ('GR','Grey')],
        validators=[DataRequired()])
    post_address = StringField('Post Address', validators=[DataRequired()])
    post_address_index = StringField('Post Address Index', validators=[DataRequired()])
    submit = SubmitField('Confirm Order')

    def validate_post_address_index(self, post_address_index):
        if len(str(post_address_index.data)) != 6:
            raise ValidationError('Post address index should be 6 digits length.')


class CommentAndRateItemForm(FlaskForm):
    comment = StringField('Your Comment', validators=[DataRequired()])
    # choices = [(i, i) for i in range(11)]
    # rate = RadioField('Rate This Item (Optional)',
    #     choices=choices,
    #     validators=[DataRequired()])
    submit = SubmitField('Comment')


class DeleteCommentForm(FlaskForm):
    submit = SubmitField('Delete Comment')
