from flask import render_template, flash, redirect, url_for, request, make_response, jsonify
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import url_parse

from app import app, db
from app.forms import LoginForm, RegistrationForm, UserEditInfoForm, \
                      OrderItemForm, CommentAndRateItemForm, DeleteCommentForm, \
                      ResetUserPasswordForm
from app.models import User, StockItem, BookedItem, Comment


@app.route('/')
@app.route('/online-shop/')
def index():
    return render_template('index.html')


@app.route('/online-shop/shop/')
def shop():
    # pull from database items represented by entity StockItem
    items = StockItem.query.all()
    return render_template('shop.html', items=items)


@app.route('/online-shop/shop/<int:item_id>', methods=['GET', 'POST'])
def item(item_id):
    item = StockItem.query.filter_by(id=item_id).first()
    form = CommentAndRateItemForm()
    delete = DeleteCommentForm()

    if form.validate_on_submit():
        if current_user.is_authenticated:
            comment = Comment(author_login=current_user.login,
                item_id=item.id, body=form.comment.data)
            db.session.add(comment)
            db.session.commit()
            flash('Comment successfully added!')
        else:
            flash('Please log in to comment items!')
        return redirect(url_for('item', item_id=item.id))

    elif delete.validate_on_submit():
        # comment = Comment.query.filter_by(id=current_user.id).first()
        comment = Comment.query.filter_by(author_login=current_user.login).first()
        db.session.delete(comment)
        db.session.commit()
        flash('Comment removed')
        return redirect(url_for('item', item_id=item.id))

    return render_template('item.html', item=item, form=form, delete=delete)


@app.route('/online-shop/shop/<int:item_id>/order', methods=['GET', 'POST'])
@login_required
def order_item(item_id):
    form = OrderItemForm()
    item = StockItem.query.filter_by(id=item_id).first()

    if form.validate_on_submit():
        item_order = BookedItem(user_id = current_user.id,
            company = item.company, item_name = item.item_name)
        item_order.set_item_id(item.id)
        item_order.set_color(color=str(request.form.get('color')))
        item_order.set_post_address(
            post_address=current_user.post_address,
            post_address_index=current_user.post_address_index)

        db.session.add(item_order)
        db.session.commit()
        flash('Order successfully added!')
        return redirect(url_for('login'))

    elif request.method == 'GET':
        form.post_address.data = current_user.post_address
        form.post_address_index.data = current_user.post_address_index

    return render_template('order_item.html', form=form, item=item)


@app.route('/online-shop/about/')
def info():
    return render_template('info.html')


@app.route('/online-shop/contacts/')
def contacts():
    return render_template('contacts.html')


@app.route('/online-shop/login/', methods=['GET', 'POST'])
def login():
    # If user already logged in
    if current_user.is_authenticated:
        # Here may be redirect for 'My Account' page
        return redirect(url_for('index'))

    # If not logged in - show login page with login form
    form = LoginForm()

    # Attempt validation on submit
    if form.validate_on_submit():
        # Search for user with given username in db
        user = User.query.filter_by(login=form.login.data).first()
        # No such username in db or incorrect password_hash

        if user is None or not user.check_password(form.password.data):
            flash('Invalid login or password!')
            # Render cleared login page
            return redirect(url_for('login'))

        # If username is correct and password_hash is correct
        # Let user log in
        login_user(user, remember=form.remember_me.data)
        # Redirect to next_page when login success
        next_page = request.args.get('next')
        # No next page or incorrect url was passed with login request
        # werkzeug.urls.url_parse(%URL%).netloc examines %the_next_page_URL%
        # to find out whether full domain name is included to the URL.
        # If so found, redirect to the 'index' page
        if not next_page or url_parse(next_page).netloc != '':
            # Redirect to home page with saved user
            next_page = url_for('index')
        return redirect(next_page)

    # Validation on submit drops False
    return render_template('login.html', form=form)


@app.route('/online-shop/reset_password', methods=['GET', 'POST'])
def reset_password():
    if current_user.is_authenticated:
        return redirect(url_for('login'))

    form = ResetUserPasswordForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=email.data).first()
        if user:
            send_password_reset_email(user)
        flash('Check {0} for password reset message'.format(user.email))
        return redirect(url_for('index'))

    return render_template('reset_password.html', form=form)


@app.route('/online-shop/register/', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(login=form.login.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Account for %s successfully created!' % form.login.data)
        login_user(current_user, remember_me=form.remember_me.data)
        return redirect(url_for('index'))
    return render_template('register.html', form=form)


@app.route('/online-shop/logout/')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/online-shop/my-account/')
@login_required
def my_account():
    acc_orders = BookedItem.query.filter_by(user_id=current_user.id).all()
    return render_template('account.html', acc_orders=acc_orders)


@app.route('/online-shop/my-account/edit-account/', methods=['GET', 'POST'])
@login_required
def edit_account():
    form = UserEditInfoForm()

    new_login = form.login.data
    new_email = form.email.data

    if form.validate_on_submit():
        current_user.login = form.login.data
        current_user.first_name = form.first_name.data
        current_user.second_name = form.second_name.data
        current_user.email = form.email.data
        current_user.post_address = form.post_address.data
        current_user.post_address_index = form.post_address_index.data

        db.session.commit()
        flash('Your personal data has been changed successfully!')
        return redirect(url_for('index'))

    elif request.method == 'GET':
        form.login.data = current_user.login
        form.first_name.data = current_user.first_name
        form.second_name.data = current_user.second_name
        form.email.data = current_user.email
        form.post_address.data = current_user.post_address
        form.post_address_index.data = current_user.post_address_index

    return render_template('edit_account.html', form=form)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': '404. Not found.'})), 404
