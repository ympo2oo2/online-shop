from flask_mail import Message
from app import mail


def send(subject, sender, recipients, body, html):
    message = Message(subject, sender=sender, recipients=recipients)
    message.body = body
    message.html = html
    mail.send(message)
