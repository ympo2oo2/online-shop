from app import db, login
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class User(UserMixin, db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(32), unique=True)
    first_name = db.Column(db.String(32))
    second_name = db.Column(db.String(32))
    email = db.Column(db.String(64), unique=True)
    post_address = db.Column(db.String(128))
    post_address_index = db.Column(db.Integer())
    orders = db.relationship('BookedItem', backref='user', lazy='joined')
    user_comments = db.relationship('Comment', backref='stockitem', lazy='joined')
    password_hash = db.Column(db.String(256))

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.login)


class BookedItem(db.Model):
    __tablename__ = 'orders'

    id = db.Column(db.Integer, primary_key=True)
    company = db.Column(db.String(64))
    item_name = db.Column(db.String(128))
    color = db.Column(db.String(32))
    order_date = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    arrival_date = db.Column(db.DateTime, default=datetime.utcnow)
    # Foreign Keys
    item_id = db.Column(db.Integer)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    post_address = db.Column(db.String(128))
    post_address_index = db.Column(db.Integer)

    def set_arrival_date(self, date):
        self.arrival_date = date

    def set_item_id(self, item_id):
        self.item_id = item_id

    def set_color(self, color):
        self.color = color

    def set_post_address(self, post_address, post_address_index):
        self.post_address = post_address
        self.post_address_index = post_address_index

    def __repr__(self):
        return '<Item {0} - {1}>'.format(self.company, self.item_name)


class StockItem(db.Model):
    __tablename__ = 'items'

    id = db.Column(db.Integer, primary_key=True)
    company = db.Column(db.String(64))
    item_name = db.Column(db.String(128))
    price = db.Column(db.Float)
    colorways = db.Column(db.String(256))
    rate = db.Column(db.Float)
    description = db.Column(db.String(256))
    is_available = db.Column(db.Boolean)
    image_path = db.Column(db.String(128))
    comments = db.relationship('Comment', backref='items', lazy='joined')

    def set_image(self, image_path):
        # convert image path to hash and set hash
        self.image_path = image_path

    def set_available(self, option):
        self.is_available = option

    def set_price(self, price):
        self.price = price

    def set_rate(self, rate):
        self.rate = rate

    def __repr__(self):
        return 'Item #{0}: {1} - {2}>'.format(
            self.id, self.company, self.item_name)


class Comment(db.Model):
    __tablename__ = 'comments'

    id = db.Column(db.Integer, primary_key=True)
    author_login = db.Column(db.String(32), db.ForeignKey('user.login'))
    item_id = db.Column(db.Integer, db.ForeignKey('items.id'))
    body = db.Column(db.String(256))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Comment #{0} on item {1} by user {2}>'.format(
            self.id, self.item_id, self.author_login)
